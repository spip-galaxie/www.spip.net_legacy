<?php

if (defined('_ECRIRE_INC_VERSION')) {
	return;
}

// hack pour ne jamais afficher les secteurs d'aide en ligne
// sauf evidemment dans le cas de l'aide en ligne, ou dans l'espace prive
define('secteurs_aide', '324');
if (!defined('aide_en_ligne')
AND !_DIR_RACINE) {
	function boucle_ARTICLES($id_boucle, &$boucles) {
		$boucles[$id_boucle]->where[] = array("'NOT IN'", "'articles.id_secteur'", '"('.secteurs_aide.')"');
		return boucle_DEFAUT_dist($id_boucle, $boucles);
	}
	function boucle_RUBRIQUES($id_boucle, &$boucles) {
		$boucles[$id_boucle]->where[] = array("'NOT IN'", "'rubriques.id_secteur'", '"('.secteurs_aide.')"');
		return boucle_DEFAUT_dist($id_boucle, $boucles);
	}
	function boucle_HIERARCHIE($id_boucle, &$boucles) {
		$boucles[$id_boucle]->where[] = array("'NOT IN'", "'rubriques.id_secteur'", '"('.secteurs_aide.')"');
		return boucle_HIERARCHIE_dist($id_boucle, $boucles);
	}
}