<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// contre correction des intertitres de galactic !
$GLOBALS['debut_intertitre'] = "\n<h3 class=\"spip\">\n";
$GLOBALS['fin_intertitre'] = "</h3>\n";

define('aide_en_ligne', 'nous y sommes');

## on veut purger les notes a chaque section
function propre_facon_aide($texte) {
	foreach (explode('<h2>', $texte) as $morceau) {
		$t = propre('<h2>' . trim($morceau));
		if ($n = $GLOBALS['les_notes']) {
			$GLOBALS['les_notes'] = '';
			$t .= "\n\n<div class='notes'><small>$n</small></div>\n";
		}
		$array[] = $t;
	}

	$texte = preg_replace(
		',^.*?<h2>,',
		'',
		join('', $array)
	);

	# reperer et recopier les images temporaires
	$max = 100;
	while (preg_match(',local/(cache[^/]*)/([^.]*.(gif|png|jpg)),', $texte, $regs) && $max--) {
		@copy($regs[0], $f = ('AIDE/local-' . $regs[1] . '-' . $regs[2]));
		$texte = str_replace($regs[0], 'https://www.spip.net/' . $f, $texte);
	}

	return $texte;
}

## transitoire : aller chercher l'aide dans le fichier aide_tmp/oc-aide.html
function aide_tmp($rien) {
	global $lang_aide;
	if (file_exists($f = "aide_tmp/$lang_aide-aide.html")) {
		return join('', file($f));
	}
	if ($lang_aide == 'pt_br')
		return file_get_contents("aide_tmp/pt-aide.html");
}
