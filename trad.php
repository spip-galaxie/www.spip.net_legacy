<?php

if (!defined("_ECRIRE_INC_VERSION")) 
  require 'ecrire/inc_version.php';

include_spip('inc/headers');
include_spip('inc/minipres');
include_spip('inc/filtres');
include_spip('base/abstract_sql');
include_spip('inc/lang_liste');


function exec_trad()
{
  echo '<html><head>' . "\n";
  echo '<title>Bilan des traductions</title>' . "\n";
  echo '<meta name="robots" content="noindex">' . "\n";
  echo '</head><body>' . "\n";
  trad_bilan();
  echo '</body></html>';
}

$GLOBALS['couleurs_statut'] = array(
	'publie'=>'green',
	'prepa'=>'grey',
	'prop'=>'orange',
	'refuse'=>'red',
	'poubelle'=>'black',
	'retard' => 'purple'
);

$GLOBALS['explication_statut'] = array(
	'grey'=>'en pr&eacute;paration&nbsp;;',
	'orange'=>'propos&eacute;e&nbsp;;',
	'red'=>'refus&eacute;e&nbsp;;',
	'black'=>'&agrave; la poubelle&nbsp;;',
	'green'=>'publi&eacute;e et &agrave jour&nbsp;;',
	'purple' => 'publi&eacute;e ant&eacute;rieurement &agrave; la derni&egrave;re modification de l\'article de r&eacute;f&eacute;rence (donc &agrave; r&eacute;viser).'
);

function trad_bilan()
{
	$afficher_non_traduits = _request('afficher_non_traduits');
	$no_url = _request('no_url');
	$langues = explode(',', $GLOBALS['meta']['langues_utilisees']);
# PHP5.2 $langues= array_fill_keys($langues, 0);
	$langues = array_flip($langues);
	$url_rubs = array();
	foreach($langues as $l => $v) {
		$langues[$l] = 0;
		$t = $GLOBALS['codes_langues'][$l];
		$h = generer_url_ecrire('rubrique', 'id_rubrique='.rub($l));
		$url_rubs[$l] = " href='" .  $h ."' title='" . $t . "'" ;
	}

	$affiche = array();
	$tt = '';
	$visible = false;
	$id_trad = $num = 0;
	$cut = $no_url ? 14 : 25;
	$w = ($afficher_non_traduits=='oui') ? "id_trad = 0" : "id_trad > 0";
	$q = sql_select('id_article, id_trad, titre, statut, date, date_modif, lang, popularite', 'spip_articles', "$w AND statut<>'poubelle' AND NOT (chapo LIKE '=%')",'',  "id_trad DESC, id_article<>id_trad, lang");

	while ($a = sql_fetch($q)) {
		$titre = (supprimer_numero($a['titre']));
		$id = $a['id_trad'];
		if (($id <> $id_trad) OR !$id) {
			if ($visible)
			  $affiche[$num][$fr_titre] = 
			    "\n\n<td>" 
			    . couper(typo($fr_titre),$cut)
			    . "</td>"
			    . join("\n", $tdd)
			    . "<td>$annee</td><td style='text-align: right'>"
			    . $popularite
			    . "</td>";

			$id_trad = $id;
			if (!$id) $id = $a['id_article'];
			$date_modif = $a['date_modif'];
			$annee=  annee($a['date']);
			foreach ($langues as $langue => $v) {
				if ($no_url)
				  $tdd[$langue] = "<td>+</td>";
				else {
				  $url = generer_url_ecrire('article_edit', "new=oui&lier_trad=$id&id_rubrique=".rub($langue));
				  $t = $GLOBALS['codes_langues'][$langue];
				  $tdd[$langue] = "<td><a href='$url' title='$t'>+</a></td>";
				}
			}
			$visible = false;
			$fr_titre = $titre;
			$popularite = $num = 0;
		}
		$num++;
		if ($a['statut'] == 'publie') $visible = true;
		$modif = $a['date_modif'];
		$couleur = (($modif < $date_modif) AND ($a['statut']=='publie')) ? 'retard' : $a['statut'];
		$couleur = $GLOBALS['couleurs_statut'][$couleur];
		$style='';
		$lang = $a['lang'];
		$td = langue_court($lang);
		if ($a['id_trad'] == $a['id_article']) {
		  $style = 'font-weight: bold;';
		}
		$p = round($a['popularite']);
		$popularite += $p;
		if (!$no_url) {
			$style .= "background-color: $couleur";
			$t = entites_html(textebrut($titre)) . ' ('
			. affdate($modif) . ') '. _T('popularite') . ' ' . $p;
			$u = generer_url_ecrire('article','id_article=' . $a['id_article']);
			$td = "<a href='$u' style='color: white' title=\"$t\">$td</a>";
		} 
		$tdd[$lang] = "<td style='text-align:center;$style'>" . $td  . "</td>";
		$langues[$lang]++;
	}

	if ($visible) 
		$affiche[$num][$fr_titre] = 
			"\n\n<td>" 
			. couper(typo($fr_titre),$cut)
			. "</td>"
			. join("\n", $tdd)
			. "<td>$annee</td><td style='text-align: right'>"
		  	. $popularite
		  	. "</td>";

	$keys = array_keys($affiche);
	// Trier par nombre de traductions
	rsort($keys);

	install_debut_html("Suivi des traductions", "documents", "articles");
	echo "<p style='text-align: left'>
 Cette page pr&eacute;sente le bilan des traductions de ce site.<br />
 La couleur d'une case correspond au statut de la traduction&nbsp;:<br /><table>";
	foreach($GLOBALS['explication_statut'] as $c => $t)
	  echo "<tr><td style='background-color: $c; color:white;'>",
	  _T($c), "</td><td>&nbsp;: $t</td></tr>"; 	
	echo "</table>";
	echo "<span style='font-weight: bold;'>En gras</span>&nbsp;: la langue de l'article de r&eacute;f&eacute;rence.<br /><br />";

	echo "<table border='0' style='font-family: Verdana,Arial,Helvetica,sans-serif; font-size:10px;'>";
	$pied = $tete = '';
	foreach ($langues as $l => $n) {
		$c = langue_court($l);
		$h = $url_rubs[$l];
		$tete .= "\n<th style='background-color: yellow'><a$h>$c</a></th>";
		$pied .= "\n<td style='text-align:right; font-size: 8px; background-color: yellow'><a$h>" .sprintf("%3d", $n) . "</a></td>";
	}
	echo "<tr><th>Titre</th>", $tete, '<th>Date</th><th>Populari&eacute;</th></tr>';
	$n = count($langues)+3; # (3 car titre, date et popularite en +)
	foreach ($keys as $num) {
	  // Trier sur le titre en francais
	  ksort($affiche[$num]);
	  echo "\n<tr id='g$num'>",
	    "<td colspan='$n' style='text-align:center; border: 1px solid black; background-color:white'>",
	    $num,
	    "</td></tr>";
	  $i = true;
	  foreach($affiche[$num] as $l){
	    $i = !$i;
	    $s = " style='background-color: " . ($i ? '#dddddd' : '#eeeeee') . "'";
	    echo "<tr$s>$l</tr>";
	  }
	}
	echo "<tr><td></td>", $pied, '<td></td><td></td></tr>';
	echo "</table>";

	if ($afficher_non_traduits === 'oui') {
	  $url = parametre_url(self(), 'afficher_non_traduits','');
	  $titre = 'Afficher les articles traduits';
	} else {
	  $url = parametre_url(self(), 'afficher_non_traduits','oui');
	  $titre = 'Afficher les articles non traduits';
	}
	echo "<div><br /><a href='$url' style='font-face: arial,helvetica,sans-serif color: black'>", $titre, "</a></div>\n";

	install_fin_html();
}

function rub($lang) {
	// cas particulier du francais
	static $rubs = array('fr' => 91); 
	if (!isset($rubs[$lang]))
		$rubs[$lang] = sql_getfetsel("id_secteur", "spip_rubriques", "id_parent=0 AND lang = '$lang' AND id_secteur<>4");
	return $rubs[$lang];
}

function langue_court($l)
{
	return $l[0] . $l[1].(preg_match('/[a-z]/', $l[2]) ? $l[2] :'');
}
#http_no_cache();
exec_trad();
